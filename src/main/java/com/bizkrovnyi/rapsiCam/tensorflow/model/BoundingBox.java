package com.bizkrovnyi.rapsiCam.tensorflow.model;

import lombok.Data;

/**
 * Model to store the data of a bounding box
 */
@Data
public class BoundingBox {
    private double x;
    private double y;
    private double width;
    private double height;
    private double confidence;
    private double[] classes;

}
