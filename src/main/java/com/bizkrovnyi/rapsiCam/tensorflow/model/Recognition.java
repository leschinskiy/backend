package com.bizkrovnyi.rapsiCam.tensorflow.model;

import lombok.Data;

/**
 * An immutable result returned by a recognizer describing what was recognized.
 */
@Data
public final class Recognition {
    /**
     * A unique identifier for what has been recognized. Specific to the class, not the instance of
     * the object.
     */
    private final Integer id;
    private final String title;
    private final Float confidence;
    private BoxPosition location;

    public Recognition(final Integer id, final String title,
                       final Float confidence, final BoxPosition location) {
        this.id = id;
        this.title = title;
        this.confidence = confidence;
        this.location = location;
    }


    public BoxPosition getScaledLocation(final float scaleX, final float scaleY) {
        return new BoxPosition(location, scaleX, scaleY);
    }

    public BoxPosition getLocation() {
        return new BoxPosition(location);
    }


    @Override
    public String toString() {
        return "Recognition{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", confidence=" + confidence +
                ", location=" + location +
                '}';
    }
}
