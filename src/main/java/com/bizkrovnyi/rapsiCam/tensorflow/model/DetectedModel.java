package com.bizkrovnyi.rapsiCam.tensorflow.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DetectedModel {
    private String name;
    private float square;
}
