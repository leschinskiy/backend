package com.bizkrovnyi.rapsiCam.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "LOCATION")
@Data
public class Location implements Serializable {

    @Id
    @Column(name = "LAT")
    @NotBlank
    private double lat;
    @Id
    @Column(name = "LNG")
    @NotBlank
    private double log;
}
