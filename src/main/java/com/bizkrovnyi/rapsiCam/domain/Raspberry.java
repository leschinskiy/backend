package com.bizkrovnyi.rapsiCam.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.net.URL;

@Entity
@Table(name = "RASPBERRY")
@Data
public class Raspberry {
    @Id
    @Column(name = "RASPBERRY_MAC")
    @NotBlank
    private String mac;
    @Column(name = "NAME")
    @NotBlank
    private String name;
}
