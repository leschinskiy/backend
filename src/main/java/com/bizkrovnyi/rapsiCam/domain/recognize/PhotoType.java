package com.bizkrovnyi.rapsiCam.domain.recognize;

public enum PhotoType {
    PORTRET, SPEED_PHOTO, LANDSCAPE, USUAL
}
