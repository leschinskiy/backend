package com.bizkrovnyi.rapsiCam.domain.recognize;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "RECOGNIZE_ENTITY")
public class RecognizeEntity {
    @Id
    @NotBlank
    @Column(name = "RECOGNIZE_ID")
    private String id;
    @NotBlank
    @Column(name = "PHOTO_TYPE")
    private PhotoType photoType;
}
