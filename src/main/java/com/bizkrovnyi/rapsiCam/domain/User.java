package com.bizkrovnyi.rapsiCam.domain;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.domain.oauth.AuthProvider;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "USER")
@Data
public class User {
    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private Long id;
    @NotBlank
    @Column(name = "NAME")
    private String name;
    @NotBlank
    @Column(name = "EMAIL")
    @Email
    private String email;
    @Column(name = "PASSWORD")
    private String password;

    @Column(nullable = false)
    private Boolean emailVerified = false;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String imageUrl;

    private String providerId;

    @OneToMany
    private List<Raspberry> raspberries;
    @OneToMany
    private List<Image> images;

    @OneToMany
    private List<CameraCharacteristics> cameraCharacteristics;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                Objects.equals(name, user.name) &&
                email.equals(user.email) &&
                password.equals(user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, password);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
