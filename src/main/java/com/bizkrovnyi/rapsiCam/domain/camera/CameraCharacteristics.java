package com.bizkrovnyi.rapsiCam.domain.camera;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "CAMERA")
@Data
public class CameraCharacteristics {
    @Id
    @Column(name = "CAMERA_MAC")
    @NotBlank
    private String mac;
    @Column(name = "PHONE_NAME")
    @NotBlank
    private String phoneName;


    @Column(name = "APERTURES")
    @ElementCollection
    private List<Double> availableApertures;


    @Column(name = "SHUTTER_TIME")
    @ElementCollection
    private List<Double> exposureRange;

    @Column(name = "ISO")
    @ElementCollection
    private List<Integer> isoRange;
}
