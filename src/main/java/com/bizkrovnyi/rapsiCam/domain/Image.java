package com.bizkrovnyi.rapsiCam.domain;

import com.bizkrovnyi.rapsiCam.domain.Location;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "IMAGE")
@Data
public class Image {
    @Id
    @Column(name = "IMAGE_ID")
    private String id;
    @Column(name = "LOCATION")
    private Location location;

    @Column(name = "APERTURE")
    private double aperture;

    @Column(name = "SHUTTER_SPEED")
    private double shutterSpeed;

    @Column(name = "ISO")
    private int iso;

    @NotBlank
    @Column(name = "BASE64", length = 999999999)
    private String base64Value;
}
