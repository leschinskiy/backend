package com.bizkrovnyi.rapsiCam.domain.oauth;

public enum AuthProvider {
    local,
    facebook,
    google,
    github
}
