package com.bizkrovnyi.rapsiCam.security;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;


@Service
public class GoogleJwtValidator {

    private static final JacksonFactory jacksonFactory = new JacksonFactory();
    private static final String CLIENT_ID = "959596938583-ijqg643lbjoq5230j20n0combspt932d.apps.googleusercontent.com";
    private GoogleIdToken.Payload payload;


    public boolean verify(String token) {

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();
        try {
            GoogleIdToken idToken = verifier.verify(token);
            if (idToken != null) {
                payload = idToken.getPayload();
                return true;
            } else {
                System.out.println("Invalid ID token.");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public String getEmail(){
       return payload.getEmail();
    }
}
