package com.bizkrovnyi.rapsiCam.repository;

import com.bizkrovnyi.rapsiCam.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image,String> {
}
