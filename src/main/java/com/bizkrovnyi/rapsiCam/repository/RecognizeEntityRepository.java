package com.bizkrovnyi.rapsiCam.repository;

import com.bizkrovnyi.rapsiCam.domain.recognize.RecognizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecognizeEntityRepository extends JpaRepository<RecognizeEntity, String> {
}
