package com.bizkrovnyi.rapsiCam.repository;

import com.bizkrovnyi.rapsiCam.domain.Raspberry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaspberryRepository extends JpaRepository<Raspberry,String> {
}
