package com.bizkrovnyi.rapsiCam.repository;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CameraCharacteristicsRepository extends JpaRepository<CameraCharacteristics,String> {
}
