package com.bizkrovnyi.rapsiCam.controller;

import com.bizkrovnyi.rapsiCam.domain.User;
import com.bizkrovnyi.rapsiCam.exception.ususal.ResourceNotFoundException;
import com.bizkrovnyi.rapsiCam.repository.UserRepository;
import com.bizkrovnyi.rapsiCam.security.CurrentUser;
import com.bizkrovnyi.rapsiCam.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }
}
