package com.bizkrovnyi.rapsiCam.controller;

import com.bizkrovnyi.rapsiCam.domain.Raspberry;
import com.bizkrovnyi.rapsiCam.domain.User;
import com.bizkrovnyi.rapsiCam.repository.UserRepository;
import com.bizkrovnyi.rapsiCam.service.analyze.utils.securityUtil.SecurityUtil;
import com.bizkrovnyi.rapsiCam.service.db.RaspberryService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RequestMapping("/api/rest/rasp")
@RestController
public class RaspberryPiController {

    private final UserRepository userRepository;

    private final RaspberryService piService;

    private final SecurityUtil securityUtil;

    @Autowired
    public RaspberryPiController(UserRepository userRepository, RaspberryService piService, SecurityUtil securityUtil) {
        this.userRepository = userRepository;
        this.piService = piService;
        this.securityUtil = securityUtil;
    }

    @GetMapping("/mac")
    public ResponseEntity getMacId() {
        return ResponseEntity.ok(new Gson().toJson(
                securityUtil.getSignedUser().getRaspberries().stream()
                        .map(Raspberry::getMac)
                        .collect(Collectors.toList())));
    }

    @GetMapping("/all")
    public ResponseEntity getAll() {
        return ResponseEntity.ok(new Gson().toJson(
                securityUtil.getSignedUser().getRaspberries().stream()
                        .map(item -> new Gson().toJson(item))
                        .collect(Collectors.toList())));
    }

    @PostMapping("/signup")
    public ResponseEntity singUpPi(@RequestBody Raspberry raspberry) {
        User signedUser = securityUtil.getSignedUser();
        boolean isExist = signedUser.getRaspberries().stream().anyMatch(item -> raspberry.getMac().equalsIgnoreCase(item.getMac()));
        if (isExist) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        piService.create(raspberry);
        signedUser.getRaspberries().add(raspberry);
        userRepository.saveAndFlush(signedUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }



}
