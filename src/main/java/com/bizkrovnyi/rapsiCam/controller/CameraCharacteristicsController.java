package com.bizkrovnyi.rapsiCam.controller;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.service.db.CameraCharacteristicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/rest")
public class CameraCharacteristicsController {

    @Autowired
    private final CameraCharacteristicsService cameraCharacteristicsService;

    public CameraCharacteristicsController(CameraCharacteristicsService cameraCharacteristicsService) {
        this.cameraCharacteristicsService = cameraCharacteristicsService;
    }


    @PostMapping("/camera")
    public ResponseEntity uploadSettings(@RequestBody CameraCharacteristics cameraCharacteristics){
       if(cameraCharacteristicsService.getById(cameraCharacteristics.getMac())!= null){
           return ResponseEntity.status(HttpStatus.CONFLICT).build();
       }
       cameraCharacteristicsService.save(cameraCharacteristics);
        return ResponseEntity.ok("Saved");
    }
}
