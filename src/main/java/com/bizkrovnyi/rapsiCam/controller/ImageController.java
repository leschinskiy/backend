package com.bizkrovnyi.rapsiCam.controller;

import com.bizkrovnyi.rapsiCam.domain.Image;
import com.bizkrovnyi.rapsiCam.domain.User;
import com.bizkrovnyi.rapsiCam.dto.androidModel.RequestDTO;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.repository.ImageRepository;
import com.bizkrovnyi.rapsiCam.repository.UserRepository;
import com.bizkrovnyi.rapsiCam.service.analyze.ComputeSettings;

import com.bizkrovnyi.rapsiCam.service.analyze.utils.securityUtil.SecurityUtil;
import com.bizkrovnyi.rapsiCam.service.db.ImageService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Base64;
import java.util.List;


@RestController
@RequestMapping("/api/rest/images")
public class ImageController {


    private final ComputeSettings computeSettings;
    private final SecurityUtil securityUtil;
    private final ImageRepository imageRepository;
    private final UserRepository userRepository;
    private final ImageService imageService;

    @Autowired
    public ImageController(ComputeSettings computeSettings, SecurityUtil securityUtil, ImageRepository imageRepository, UserRepository userRepository, ImageService imageService) {
        this.computeSettings = computeSettings;
        this.securityUtil = securityUtil;
        this.imageRepository = imageRepository;
        this.userRepository = userRepository;
        this.imageService = imageService;
    }

    @PostMapping("/analyze")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity handleFileUpload(@RequestParam("img") String file, @RequestParam(name = "model") String requestModel) throws IOException {
        RequestDTO requestDTO = new Gson().fromJson(requestModel, RequestDTO.class);
        String imgBase64 = file.substring(1, file.length() - 1);
        ResponseDTO response = computeSettings.calculate(Base64.getDecoder().decode(imgBase64), requestDTO);
        Image image = imageService.createImage(imgBase64,response, requestDTO.getLocation());
        imageRepository.save(image);

        User signedUser = securityUtil.getSignedUser();
        List<Image> imageList = signedUser.getImages();
        imageList.add(image);
        signedUser.setImages(imageList);
        userRepository.saveAndFlush(signedUser);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getAll")
    public Integer getCount() {
        return securityUtil.getSignedUser().getImages().size();
    }


}
