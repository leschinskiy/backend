package com.bizkrovnyi.rapsiCam.dto.features;

import lombok.Data;

@Data
public class WeatherDTO {
    private String city;
    private String country;
    private String temperature;
    private String sunset;
    private String sunrise;
}
