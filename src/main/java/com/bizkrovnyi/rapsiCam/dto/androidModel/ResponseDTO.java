package com.bizkrovnyi.rapsiCam.dto.androidModel;

import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import lombok.Data;

@Data
public class ResponseDTO {
    private double aperture;
    private double shutterTime;
    private int iso;
    private String advice;
    private PhotoType photoType;
}
