package com.bizkrovnyi.rapsiCam.dto.androidModel;

import com.bizkrovnyi.rapsiCam.domain.Location;
import lombok.Data;

@Data
public class RequestDTO {
    private Location location;
    private String macAddress;
}
