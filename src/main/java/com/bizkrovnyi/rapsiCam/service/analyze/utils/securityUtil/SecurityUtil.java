package com.bizkrovnyi.rapsiCam.service.analyze.utils.securityUtil;

import com.bizkrovnyi.rapsiCam.domain.User;
import com.bizkrovnyi.rapsiCam.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class SecurityUtil {

    @Autowired
    private UserRepository userRepository;

    public User getSignedUser() {
        UserDetails userDetails =
                (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new IllegalArgumentException("User with this email does not exist"));
    }
}
