package com.bizkrovnyi.rapsiCam.service.analyze.utils;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Slf4j
public class ImageUtils {

    private static BufferedImage bufferedImage;

    public static double getLuminance(byte[] image) {
        try {
            double avgLum = 0;
            bufferedImage = ImageIO.read(prepareImageFile(image));

            for (int i = 0; i < bufferedImage.getWidth(); i++) {
                for (int j = 0; j < bufferedImage.getHeight(); j++) {
                    int pixelCol = bufferedImage.getRGB(i, j);
                    int r = (pixelCol >>> 16) & 0xff;
                    int g = (pixelCol >>> 8) & 0xff;
                    int b = pixelCol & 0xff;
                    avgLum += ((0.2126 * r) + (0.7152 * g) + (0.0722 * b)) / 255;
                }
            }
            return avgLum / bufferedImage.getHeight();
        } catch (IOException e) {
            log.error("Can`t get luminance");
            throw new IllegalArgumentException("Can`t get luminance");
        }
    }

    private static File prepareImageFile(byte[] imageArray) {
        try {
            File imageFile = File.createTempFile("image", "jpeg");
            FileUtils.writeByteArrayToFile(imageFile, imageArray);
            return imageFile;
        } catch (IOException e) {
            log.error("Can not create file from byte array", e);
            throw new IllegalArgumentException("Can not create file from byte array");
        }
    }

    public static int getRoundIso(double isoDouble) {
        int prevIso = 0;
        for (int iso = 50; ; iso *= 2) {
            if (iso >= isoDouble) {
                if (isoDouble - prevIso <= iso - isoDouble) {
                    return prevIso;
                }
                return iso;
            }
            prevIso = iso;
        }
    }

    private static Set<Double> generateApertureListFromRange(List<Double> smartphoneApertures) {
        Set<Double> apertures = new TreeSet<>();
        apertures.add(1.4);
        apertures.add(2d);
        apertures.add(2.8);
        apertures.add(4d);
        apertures.add(5.6);
        apertures.add(8d);
        apertures.add(11d);
        apertures.add(16d);
        apertures.add(22d);
        apertures.add(32d);
        Set<Double> sortedApertures = new TreeSet<>();
        apertures.forEach(item -> {
            if(item > smartphoneApertures.get(0) && item <= smartphoneApertures.get(1)){
                sortedApertures.add(item);
            }
        });
        return sortedApertures;
    }


    public static double getNearAperture(List<Double> smartphoneApertures, double needAperture) {
            Set<Double> generatedApertureSet = generateApertureListFromRange(smartphoneApertures);
            for (Double aperture : generatedApertureSet) {
                if (aperture >= needAperture) {
                    return aperture;
                }
            }
        return smartphoneApertures.get(1);
    }

    public static int getNearIso(List<Integer> phonesIso, int needIso) {
        for (Integer iso : phonesIso) {
            if (iso >= needIso) {
                return iso;
            }
        }
        return phonesIso.get(phonesIso.size() - 1);
    }

}
