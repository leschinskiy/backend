package com.bizkrovnyi.rapsiCam.service.analyze.modes;

import com.bizkrovnyi.rapsiCam.config.ApplicationProperties;
import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.service.analyze.utils.ImageUtils;

import java.util.List;

public class LandscapeMode extends Mode {

    private double luminance;
    private CameraCharacteristics characteristics;
    private ApplicationProperties properties;
    private ResponseDTO responseDTO;

    public LandscapeMode(double luminance, CameraCharacteristics characteristics) {
        this.luminance = luminance;
        this.characteristics = characteristics;
        setMinAperture(11);
        setMinIso(1600);
        setMinShutterSec(1 / 60.);
        responseDTO = new ResponseDTO();
    }

    @Override
    public ResponseDTO getResponse() {
        responseDTO.setPhotoType(PhotoType.LANDSCAPE);
        List<Double> apertures = characteristics.getAvailableApertures();
        double aperture = ImageUtils.getNearAperture(apertures,getMinAperture());
        double shutter = MIN_SHUTTER_SMART_PHONE;
        double iso = ((Math.pow(aperture, 2) * CALIBRATE_CONST) / shutter) / luminance;

        responseDTO.setIso(ImageUtils.getRoundIso(iso));
        responseDTO.setAperture(aperture);
        responseDTO.setShutterTime(1 / shutter);
        return responseDTO;
    }
}
