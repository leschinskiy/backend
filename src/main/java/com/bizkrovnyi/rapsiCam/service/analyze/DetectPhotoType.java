package com.bizkrovnyi.rapsiCam.service.analyze;

import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import com.bizkrovnyi.rapsiCam.domain.recognize.RecognizeEntity;
import com.bizkrovnyi.rapsiCam.service.db.RecognizeEntityService;
import com.bizkrovnyi.rapsiCam.tensorflow.model.DetectedModel;
import com.bizkrovnyi.rapsiCam.tensorflow.service.classifier.ObjectDetector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Service
public class DetectPhotoType {

    private final ObjectDetector objectDetector;
    private final RecognizeEntityService entityService;
    private static final int MODEL_LIMIT = 3;


    @Autowired
    public DetectPhotoType(ObjectDetector objectDetector, RecognizeEntityService entityService) {
        this.objectDetector = objectDetector;
        this.entityService = entityService;
    }

    private List<RecognizeEntity> findRecognizeEntities(List<DetectedModel> top) {
        return top.stream()
                .map(model -> createRecognizeEntity(model.getName()))
                .collect(Collectors.toList());
    }

    private RecognizeEntity createRecognizeEntity(String id) {
        return entityService.getById(id);
    }

    public PhotoType getDetectedPhotoType(byte[] image) {
        Comparator<DetectedModel> squareComparator = Comparator.comparing(DetectedModel::getSquare, Comparator.reverseOrder());

        List<DetectedModel> topDetected = objectDetector.detect(image).stream()
                .sorted(squareComparator)
                .limit(MODEL_LIMIT)
                .collect(Collectors.toList());

        List<RecognizeEntity> entities = findRecognizeEntities(topDetected);

        return calculateAvgType(entities);
    }


    private PhotoType calculateAvgType(List<RecognizeEntity> recognizeEntities) {
        if (recognizeEntities.isEmpty()) {
            return PhotoType.USUAL;
        }
        PhotoType sameEntity = sameEntities(recognizeEntities);
        if (sameEntity != null) {
            return sameEntity;
        }

        TreeMap<Integer, PhotoType> avg = new TreeMap<>();
        AtomicInteger count = new AtomicInteger();

        recognizeEntities.forEach(entity -> {
            PhotoType type = entity.getPhotoType();

            recognizeEntities.forEach(item -> {
                if (item.getPhotoType() == type) {
                    count.getAndIncrement();
                }
            });
            avg.put(count.get(), type);
        });

        return avg.firstEntry().getValue();
    }

    private PhotoType sameEntities(List<RecognizeEntity> recognizeEntities) {

        int countDistinctEntity = recognizeEntities.stream().distinct().collect(Collectors.toList()).size();
        if (recognizeEntities.size() > 5 && countDistinctEntity == 1) {
            return PhotoType.LANDSCAPE;
        }
        if (countDistinctEntity >= 2) {
            return PhotoType.LANDSCAPE;
        }
        return null;
    }
}
