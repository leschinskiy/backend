package com.bizkrovnyi.rapsiCam.service.analyze.modes;

import com.bizkrovnyi.rapsiCam.config.ApplicationProperties;
import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;

public class UsualMode extends Mode {

    private double luminance;
    private CameraCharacteristics characteristics;
    private ApplicationProperties properties;
    private ResponseDTO response;


    public UsualMode(double luminance, CameraCharacteristics characteristics) {
        this.luminance = luminance;
        this.characteristics = characteristics;
        setMinAperture(0);
        setMinIso(0);
        setMinShutterSec(0);
        response = new ResponseDTO();
        response.setAdvice("Use standard camera`s setting");
    }

    @Override
    public ResponseDTO getResponse() {
        response.setAperture(getMinAperture());
        response.setShutterTime(getMinShutterSec());
        response.setIso(getMinIso());
        return response;
    }
}
