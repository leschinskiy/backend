package com.bizkrovnyi.rapsiCam.service.analyze.modes;

import com.bizkrovnyi.rapsiCam.config.ApplicationProperties;
import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.service.analyze.utils.ImageUtils;

import java.util.List;

public class SpeedMode extends Mode {

    private double luminance;
    private CameraCharacteristics characteristics;
    private ApplicationProperties properties;
    ResponseDTO response;


    public SpeedMode(double luminance, CameraCharacteristics characteristics) {
        this.luminance = luminance;
        this.characteristics = characteristics;
        setMinAperture(4);
        setMinIso(3200);
        setMinShutterSec(1 / 1000.);
        response = new ResponseDTO();
    }


    @Override
    public ResponseDTO getResponse() {
        response.setPhotoType(PhotoType.SPEED_PHOTO);
        List<Double> shutters = characteristics.getExposureRange();
        double mostShutter = 1 / (shutters.get(shutters.size() - 1) / Math.pow(10,9));
        double mostAperture = ImageUtils.getNearAperture(characteristics.getAvailableApertures(), getMinAperture());

        double iso = ((Math.pow(mostAperture, 2) * CALIBRATE_CONST) /(1 / mostShutter)) / luminance;
        response.setShutterTime(mostShutter);
        response.setAperture(mostAperture);
        response.setIso(ImageUtils.getRoundIso(iso));
        return response;
    }
}
