package com.bizkrovnyi.rapsiCam.service.analyze;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import com.bizkrovnyi.rapsiCam.dto.androidModel.RequestDTO;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.service.analyze.modes.LandscapeMode;
import com.bizkrovnyi.rapsiCam.service.analyze.modes.PortretMode;
import com.bizkrovnyi.rapsiCam.service.analyze.modes.SpeedMode;
import com.bizkrovnyi.rapsiCam.service.analyze.modes.UsualMode;
import com.bizkrovnyi.rapsiCam.service.db.CameraCharacteristicsService;
import com.bizkrovnyi.rapsiCam.service.analyze.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComputeSettings {

    private final DetectPhotoType detector;
    private final CameraCharacteristicsService cameraService;


    @Autowired
    public ComputeSettings(DetectPhotoType type, CameraCharacteristicsService cameraService) {
        this.detector = type;
        this.cameraService = cameraService;
    }

    public ResponseDTO calculate(byte[] imageArray, RequestDTO requestModel) {
        PhotoType photoType = detector.getDetectedPhotoType(imageArray);
        double luminance = ImageUtils.getLuminance(imageArray);
        CameraCharacteristics characteristics = cameraService.getById(requestModel.getMacAddress());
        switch (photoType) {
            case PORTRET: {
                return new PortretMode(luminance, characteristics).getResponse();
            }
            case SPEED_PHOTO: {
                return new SpeedMode(luminance, characteristics).getResponse();
            }
            case LANDSCAPE: {
                return new LandscapeMode(luminance, characteristics).getResponse();
            }
            case USUAL: {
                return new UsualMode(luminance, characteristics).getResponse();
            }
            default: {
                throw new IllegalArgumentException("Photo type did not found.");
            }
        }
    }


}
