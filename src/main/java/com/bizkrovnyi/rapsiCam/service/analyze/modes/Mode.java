package com.bizkrovnyi.rapsiCam.service.analyze.modes;

import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import lombok.Data;

@Data
public abstract class Mode {
    public static double MIN_SHUTTER_SMART_PHONE = 0.045;
    public static double CALIBRATE_CONST = 217.89966;
    private double minAperture = 1;
    private int minIso = 100;
    private double minShutterSec = 1800;

    public abstract ResponseDTO getResponse();
}
