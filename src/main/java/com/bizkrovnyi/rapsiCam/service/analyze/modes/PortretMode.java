package com.bizkrovnyi.rapsiCam.service.analyze.modes;

import com.bizkrovnyi.rapsiCam.config.ApplicationProperties;
import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.domain.recognize.PhotoType;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.service.analyze.utils.ImageUtils;


public class PortretMode extends Mode {

    private double luminance;
    private CameraCharacteristics characteristics;
    private ApplicationProperties properties;
    ResponseDTO responseModel;

    public PortretMode(double luminance, CameraCharacteristics characteristics) {
        this.luminance = luminance;
        this.characteristics = characteristics;
        setMinAperture(1.4);
        setMinIso(100);
        setMinShutterSec(1 / 60.);
        responseModel = new ResponseDTO();
    }

    @Override
    public ResponseDTO getResponse() {
        return computeShutterTime(characteristics, luminance);
    }

    private ResponseDTO computeShutterTime(CameraCharacteristics cameraCharacteristics, double luminance) {

        double minAperture = cameraCharacteristics
                .getAvailableApertures().get(0);
        responseModel.setAperture(minAperture);
        responseModel.setIso(getMinIso());
        responseModel.setPhotoType(PhotoType.PORTRET);

        if (minAperture == getMinAperture()) {
            responseModel.setShutterTime(1 / getMinShutterSec());
            return responseModel;
        }
        double shutterTime = (Math.pow(minAperture, 2) * CALIBRATE_CONST) / (getMinIso() * luminance);

        if (shutterTime > MIN_SHUTTER_SMART_PHONE) {
            double iso = ((CALIBRATE_CONST * Math.pow(minAperture, 2)) / MIN_SHUTTER_SMART_PHONE) / luminance;
            responseModel.setShutterTime(1 / MIN_SHUTTER_SMART_PHONE);
            responseModel.setIso(ImageUtils.getRoundIso(iso));
            return responseModel;
        } else {
            responseModel.setShutterTime(1 / shutterTime);
            return responseModel;
        }
    }

}
