package com.bizkrovnyi.rapsiCam.service.db;

import com.bizkrovnyi.rapsiCam.domain.Raspberry;

public interface RaspberryService {
    Raspberry create(Raspberry raspberry);
    Raspberry update(Raspberry update);

}
