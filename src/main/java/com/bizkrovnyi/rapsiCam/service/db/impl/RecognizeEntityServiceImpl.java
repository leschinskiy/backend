package com.bizkrovnyi.rapsiCam.service.db.impl;

import com.bizkrovnyi.rapsiCam.domain.recognize.RecognizeEntity;
import com.bizkrovnyi.rapsiCam.repository.RecognizeEntityRepository;
import com.bizkrovnyi.rapsiCam.service.db.RecognizeEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RecognizeEntityServiceImpl implements RecognizeEntityService {

    private final RecognizeEntityRepository entityRepository;

    @Autowired
    public RecognizeEntityServiceImpl(RecognizeEntityRepository entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    public RecognizeEntity getById(String name) {
        return entityRepository.findById(name).orElseThrow(() -> new IllegalArgumentException("Name was not found"));
    }
}
