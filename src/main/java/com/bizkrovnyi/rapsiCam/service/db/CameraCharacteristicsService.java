package com.bizkrovnyi.rapsiCam.service.db;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;

public interface CameraCharacteristicsService {
    CameraCharacteristics getById(String macAddress);
    void save(CameraCharacteristics cameraCharacteristics);
}
