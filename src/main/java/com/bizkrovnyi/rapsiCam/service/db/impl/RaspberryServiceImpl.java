package com.bizkrovnyi.rapsiCam.service.db.impl;

import com.bizkrovnyi.rapsiCam.domain.Raspberry;
import com.bizkrovnyi.rapsiCam.repository.RaspberryRepository;
import com.bizkrovnyi.rapsiCam.service.db.RaspberryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RaspberryServiceImpl  implements RaspberryService {

    @Autowired
    private RaspberryRepository raspberryRepository;

    @Override
    public Raspberry create(Raspberry raspberry) {
        return raspberryRepository.saveAndFlush(raspberry);
    }

    @Override
    public Raspberry update(Raspberry update) {
        return raspberryRepository.saveAndFlush(update);
    }
}
