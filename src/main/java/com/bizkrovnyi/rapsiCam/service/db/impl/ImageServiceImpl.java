package com.bizkrovnyi.rapsiCam.service.db.impl;

import com.bizkrovnyi.rapsiCam.domain.Image;
import com.bizkrovnyi.rapsiCam.domain.Location;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;
import com.bizkrovnyi.rapsiCam.service.db.ImageService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ImageServiceImpl implements ImageService {
    @Override
    public Image createImage(String base64, ResponseDTO response, Location location) {
        Image image = new Image();
        image.setAperture(response.getAperture());
        image.setShutterSpeed(response.getShutterTime());
        image.setId(UUID.randomUUID().toString());
        image.setIso(response.getIso());
        image.setLocation(location);
        image.setBase64Value(base64);
        return image;
    }
}
