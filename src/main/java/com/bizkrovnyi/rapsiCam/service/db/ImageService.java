package com.bizkrovnyi.rapsiCam.service.db;

import com.bizkrovnyi.rapsiCam.domain.Image;
import com.bizkrovnyi.rapsiCam.domain.Location;
import com.bizkrovnyi.rapsiCam.dto.androidModel.ResponseDTO;

public interface ImageService {
    Image createImage(String base64, ResponseDTO image, Location location);
}
