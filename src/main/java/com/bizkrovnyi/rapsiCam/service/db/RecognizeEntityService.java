package com.bizkrovnyi.rapsiCam.service.db;

import com.bizkrovnyi.rapsiCam.domain.recognize.RecognizeEntity;

public interface RecognizeEntityService {
    RecognizeEntity getById(String name);
}
