package com.bizkrovnyi.rapsiCam.service.db.impl;

import com.bizkrovnyi.rapsiCam.domain.camera.CameraCharacteristics;
import com.bizkrovnyi.rapsiCam.repository.CameraCharacteristicsRepository;
import com.bizkrovnyi.rapsiCam.service.db.CameraCharacteristicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CameraCharacteristicsServiceImpl implements CameraCharacteristicsService {

    private final CameraCharacteristicsRepository repository;

    @Autowired
    public CameraCharacteristicsServiceImpl(CameraCharacteristicsRepository repository) {
        this.repository = repository;
    }

    @Override
    public CameraCharacteristics getById(String macAddress) {
        return repository
                .findById(macAddress)
                .orElse(null);
    }

    @Override
    public void save(CameraCharacteristics cameraCharacteristics) {
        repository.saveAndFlush(cameraCharacteristics);
    }
}
