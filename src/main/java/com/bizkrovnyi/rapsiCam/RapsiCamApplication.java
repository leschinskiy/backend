package com.bizkrovnyi.rapsiCam;

import com.bizkrovnyi.rapsiCam.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class RapsiCamApplication {

	public static void main(String[] args) {
		SpringApplication.run(RapsiCamApplication.class, args);
	}


}
