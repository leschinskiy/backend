package com.bizkrovnyi.rapsiCam.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "application.auth")
@Component
public class Auth {
    private String tokenSecret;
    private long tokenExpirationMsec;
}