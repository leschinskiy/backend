package com.bizkrovnyi.rapsiCam.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "application.auth.oauth2")
public class OAuth2 {
    private List<String> authorizedRedirectUris;
}