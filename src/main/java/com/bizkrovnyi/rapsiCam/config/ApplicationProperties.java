package com.bizkrovnyi.rapsiCam.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application")
@Data
public class ApplicationProperties {
    private String graph;
    private String label;
    private String outputDir;
    private String uploadDir;
    private Integer imageSize;
    private Float imageMean;
    private String weatherAPI;
    private double minShutterSpeedSmartPhone;
    private String tokenSecret;
    private long tokenExpirationMsec;
}
